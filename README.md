A high voltage programmer for rescuing ATTiny microcontrollers.

- Works via ISP programmer interface + one jumper needed for the CLK pin. 
- Includes adapted Arduino sketch, which waits for the chip ID before burning fuses (to make it work better using fiddly ISP pogo pins).
- Also added CNC printed circuit board files to easily slot into Arduino.

![](https://gitlab.com/nebogeo/hvprog/raw/6c750012bd071fd8ad411b0f443687434377e24d/pics/setup.jpg)