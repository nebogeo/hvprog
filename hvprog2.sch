EESchema Schematic File Version 4
LIBS:hvprog2-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:AVR-ISP-6 J1
U 1 1 5D333759
P 4900 3150
F 0 "J1" H 4621 3246 50  0000 R CNN
F 1 "AVR-ISP-6" H 4621 3155 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" V 4650 3200 50  0001 C CNN
F 3 " ~" H 3625 2600 50  0001 C CNN
	1    4900 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5D333F9C
P 6600 3250
F 0 "R6" V 6393 3250 50  0000 C CNN
F 1 "R" V 6484 3250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6530 3250 50  0001 C CNN
F 3 "~" H 6600 3250 50  0001 C CNN
	1    6600 3250
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5D33431A
P 5800 2400
F 0 "R1" V 5593 2400 50  0000 C CNN
F 1 "R" V 5684 2400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5730 2400 50  0001 C CNN
F 3 "~" H 5800 2400 50  0001 C CNN
	1    5800 2400
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5D33457D
P 5800 2500
F 0 "R2" V 5593 2500 50  0000 C CNN
F 1 "R" V 5684 2500 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5730 2500 50  0001 C CNN
F 3 "~" H 5800 2500 50  0001 C CNN
	1    5800 2500
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5D334587
P 5900 2600
F 0 "R3" V 5693 2600 50  0000 C CNN
F 1 "R" V 5784 2600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5830 2600 50  0001 C CNN
F 3 "~" H 5900 2600 50  0001 C CNN
	1    5900 2600
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5D335AFF
P 6150 3650
F 0 "R5" V 5943 3650 50  0000 C CNN
F 1 "R" V 6034 3650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6080 3650 50  0001 C CNN
F 3 "~" H 6150 3650 50  0001 C CNN
	1    6150 3650
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x07_Male J4
U 1 1 5D3373F8
P 6400 2600
F 0 "J4" H 6372 2532 50  0000 R CNN
F 1 "Arduino" H 6372 2623 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x07_P2.54mm_Vertical" H 6400 2600 50  0001 C CNN
F 3 "~" H 6400 2600 50  0001 C CNN
	1    6400 2600
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5D33861D
P 6050 3250
F 0 "J3" H 6158 3431 50  0000 C CNN
F 1 "12V power" H 6158 3340 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6050 3250 50  0001 C CNN
F 3 "~" H 6050 3250 50  0001 C CNN
	1    6050 3250
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0101
U 1 1 5D338E0E
P 4800 3550
F 0 "#PWR0101" H 4800 3300 50  0001 C CNN
F 1 "Earth" H 4800 3400 50  0001 C CNN
F 2 "" H 4800 3550 50  0001 C CNN
F 3 "~" H 4800 3550 50  0001 C CNN
	1    4800 3550
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0102
U 1 1 5D33B94F
P 6250 3350
F 0 "#PWR0102" H 6250 3100 50  0001 C CNN
F 1 "Earth" H 6250 3200 50  0001 C CNN
F 2 "" H 6250 3350 50  0001 C CNN
F 3 "~" H 6250 3350 50  0001 C CNN
	1    6250 3350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6250 3250 6450 3250
$Comp
L Transistor_BJT:2N3055 Q1
U 1 1 5D33D513
P 6650 3650
F 0 "Q1" H 6840 3696 50  0000 L CNN
F 1 "2N3055" H 6840 3605 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92L_Inline_Wide" H 6850 3575 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N3055-D.PDF" H 6650 3650 50  0001 L CNN
	1    6650 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 3250 6750 3450
Wire Wire Line
	4800 2650 4800 2300
Wire Wire Line
	4800 2300 6200 2300
Wire Wire Line
	5300 3050 5500 3050
Wire Wire Line
	5500 3050 5500 2400
Wire Wire Line
	5500 2400 5650 2400
Wire Wire Line
	5950 2400 6200 2400
Wire Wire Line
	5300 2950 5650 2950
Wire Wire Line
	5650 2950 5650 2500
Wire Wire Line
	5950 2500 6200 2500
Wire Wire Line
	6050 2600 6200 2600
Wire Wire Line
	5750 3150 5300 3150
Wire Wire Line
	5750 2600 5750 3150
$Comp
L Device:R R4
U 1 1 5D335AF5
P 5950 2700
F 0 "R4" V 5743 2700 50  0000 C CNN
F 1 "R" V 5834 2700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5880 2700 50  0001 C CNN
F 3 "~" H 5950 2700 50  0001 C CNN
	1    5950 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	6100 2700 6200 2700
$Comp
L Connector:Conn_01x01_Male J2
U 1 1 5D34C557
P 4950 1800
F 0 "J2" H 5058 1981 50  0000 C CNN
F 1 "ATTiny CLK" H 5058 1890 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 4950 1800 50  0001 C CNN
F 3 "~" H 4950 1800 50  0001 C CNN
	1    4950 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1800 5150 2700
Wire Wire Line
	5150 2700 5800 2700
$Comp
L power:Earth #PWR0103
U 1 1 5D34D5A9
P 6200 2900
F 0 "#PWR0103" H 6200 2650 50  0001 C CNN
F 1 "Earth" H 6200 2750 50  0001 C CNN
F 2 "" H 6200 2900 50  0001 C CNN
F 3 "~" H 6200 2900 50  0001 C CNN
	1    6200 2900
	0    1    1    0   
$EndComp
$Comp
L power:Earth #PWR0104
U 1 1 5D35A78D
P 6750 3850
F 0 "#PWR0104" H 6750 3600 50  0001 C CNN
F 1 "Earth" H 6750 3700 50  0001 C CNN
F 2 "" H 6750 3850 50  0001 C CNN
F 3 "~" H 6750 3850 50  0001 C CNN
	1    6750 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3650 6450 3650
Wire Wire Line
	6000 3650 5900 3650
Wire Wire Line
	5900 3650 5900 2800
Wire Wire Line
	5900 2800 6200 2800
Wire Wire Line
	6750 3450 5300 3450
Wire Wire Line
	5300 3450 5300 3250
Connection ~ 6750 3450
Text Label 5600 3450 0    50   ~ 0
rst
Text Label 5750 3150 0    50   ~ 0
sck
Text Label 5300 3050 0    50   ~ 0
mosi
Text Label 5650 2950 0    50   ~ 0
miso
Text Label 4800 2550 0    50   ~ 0
vcc
Text Label 5900 2950 0    50   ~ 0
pin13
Text Label 6100 2700 0    50   ~ 0
pin12
Text Label 6050 2600 0    50   ~ 0
pin11
Text Label 5950 2500 0    50   ~ 0
pin10
Text Label 5950 2400 0    50   ~ 0
pin9
Text Label 4800 2300 0    50   ~ 0
pin8or7
$EndSCHEMATC
